﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SIEL.Startup))]
namespace SIEL
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
