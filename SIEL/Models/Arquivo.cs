﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;

namespace SIEL.Models
{
    public class Arquivo
    {
        public int ArquivoId { get; set; }
        public string Nome { get; set; }
        public string Caminho { get; set; }
        public string Tipo { get; set; }

        public int Tamanho { get; set; }


        /// <summary>
        /// Captura todos as informações do arquivo e as colocam em objetos e em seguida exibe para o usuário.
        /// </summary>
        /// <param name="excelWorksheet">Recebe um DataSet com todos os dados do arquivo</param>
        public IList<Beneficiario> Buscar(DataSet ds)
        {
            var listaBeneficiarios = new List<Beneficiario>();
            int posicao = 1;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                //ValidaArquivo(item, posicao);
                var ben = new Beneficiario
                {
                    Re = row[0].ToString(),
                    Nome = row[1].ToString(),
                    // Situacao = item[3].ToString(),
                    Unidade = row[3].ToString(),
                    Municipio = System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(row[5].ToString().ToLower())
                };
                //Adicionar as informações do beneficiario NÃO cancelado na folha
                //if (ben.Situacao.ToUpper() == "F")
                //{
                //Se o beneficiário já EXISTIR na lista
                //if (listaBeneficiarios.Any(x => x.Re == ben.Re))
                //    throw new Exception("ERRO NA LINHA " + posicao + ". O RE '" + ben.Re + "' ESTÁ DUPLICADO");
                listaBeneficiarios.Add(ben);
                //}
                posicao++;
            }
            //Retornar todos beneficiarios ordenados pelo nome
            return listaBeneficiarios
                .OrderBy(x => x.Nome)
                .ToList();
            //return listaBeneficiarios.OrderBy(x => x.Nome).Where( z => z.Unidade.ToUpper().Contains("PM")).ToList();
        }

        /// <summary>
        /// Verifica se o arquivo est� correto
        /// </summary>
        /// <param name="dr"></param>
        private void ValidaArquivo(DataRow dr, int posicao)
        {
            var re = dr[1].GetType().Name;
            var nome = dr[2].GetType().Name;
            var situacao = dr[3].GetType().Name;

            if (re.ToUpper() != "DOUBLE")
                throw new Exception("Erro na linha " + posicao + ". O campo REGISTRO está em formato incorreto. Deve ser um número.");
            if (nome.ToUpper() != "STRING")
                throw new Exception("Erro na linha " + posicao + ". O campo NOME está em formato incorreto. Deve ser um texto.");
            if (situacao.ToUpper() != "STRING")
                throw new Exception("Erro na linha " + posicao + ". O campo SITUAÇÃO está em formato incorreto. Deve ser um texto.");
        }

        /// <summary>
        /// Salva o arquivo no servidor
        /// </summary>
        /// <param name="file">Arquivo que est� sendo enviado</param>
        /// <param name="caminho">Local onde ser� salvo o arquivo</param>
        /// <returns>endere�o exato do arquivo</returns>
        public string EnviarArquivo(FileUpload file, string caminho)
        {
            string nome = Path.GetFileName(file.FileName);
            file.SaveAs(caminho + nome);
            return nome;
        }

        /// <summary>
        /// Ler os dados do arquivo para poderem ser usados
        /// </summary>
        /// <param name="nomeArquivo">Nome que o arquivo recebeu</param>
        /// <param name="caminho">Local onde foi armazenado o arquivo</param>
        /// <returns>retorna um DataSet com todos os dados contidos no arquivo</returns>
        public DataSet LerArquivo(string nomeArquivo, string caminho, out string city)
        {
            var settings = ConfigurationManager.AppSettings;
            var extended = settings["Extended"].ToString();
            var provider = settings["Provider"].ToString();
            var connectionString = $"{provider}{caminho}{nomeArquivo}{extended}";

            OleDbConnection connExcel = new OleDbConnection(connectionString);

            OleDbCommand cmdExcel = new OleDbCommand
            {
                Connection = connExcel
            };
            connExcel.Open();

            DataTable table = connExcel.GetSchema("Tables");
            var sheets = table.Rows[0];

            DataSet ds = new DataSet();
            OleDbCommand excelCommand = new OleDbCommand(); OleDbDataAdapter excelDataAdapter = new OleDbDataAdapter();
            //get "sFilePath" Excel Path
            OleDbConnection excelConn = new OleDbConnection(connectionString);
            excelConn.Open();
            //var dtPatterns = new DataTable();
            DataTable dtsheet = new DataTable();
            dtsheet = excelConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            string ExcelSheetName = dtsheet.Rows[0]["Table_Name"].ToString();
            var names = nomeArquivo.Split('.');
            var extension = names[names.Length - 1];
            var fileNameOnly = nomeArquivo.Replace($".{extension}", "");
            city = fileNameOnly;

            string query = $"SELECT * FROM [{fileNameOnly}$]";

            DataTable testTable = connExcel.GetSchema("Tables");
            DataSet excelDataSet = new DataSet();
            OleDbDataAdapter da = new OleDbDataAdapter(query, connectionString);

            da.Fill(excelDataSet);

            return excelDataSet;
        }

        /*
        public IList<Linha> buscarLinhas(string arg, string tipo) 
        {
            try
            {
                IList<Linha> lista = new List<Linha>();
                switch (tipo)
                {
                    case "1":
                        DateTime data = DateTime.Parse(arg);
                        lista = banco.Selos.Where(s => s.data == data).ToList();
                        break;
                    case "2":
                        //long num = long.Parse(arg);
                        lista = banco.Selos.Where(s => s.numero == arg).ToList();
                        break;
                    case "3":
                        lista = banco.Selos.Where(s => s.cidade == arg).ToList();
                        break;
                }
                return lista;
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }

            */

    }
}