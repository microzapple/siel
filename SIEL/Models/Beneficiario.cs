﻿
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SIEL.Models
{
    public class Beneficiario
    {
        public int BeneficiarioId { get; set; }
        public string Nome { get; set; }
        public string Re { get; set; }
        public string Situacao { get; set; }
        public string Unidade { get; set; }
        public string Municipio { get; set; }
    }
}