﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SIEL.Models
{
    public class FolhaVotacaoViewModel
    {
        public int ArquivoId { get; set; }

        public IList<Beneficiario> Beneficiarios { get; set; }

        public string Turno { get; set; }

        [Required, DisplayName("Data")]
        public DateTime DataVotacao { get; set; }

        public string Zona { get; set; }

        [DisplayName("Seção")]
        public string Secao { get; set; }

        public string Municipio { get; set; }

        [Required, DisplayName("Deseja Paginação?")]
        public Paginacao Paginacao { get; set; }
        
        [Required, DisplayName("Unidade")]
        public Unidade Unidade { get; set; }
    }
}