﻿using OpenHtmlToPdf;
using SIEL.Helpers;
using SIEL.Models;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SIEL.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string arquivo)
        {
            ViewBag.Caminho = arquivo;
            return View();
        }

        public ActionResult Arquivos()
        {
            try
            {
                var lista = db.Arquivos.ToList();
                return View(lista);
            }
            catch (Exception exception)
            {
                this.error(exception.Message);
                return View();
            }
        }

        public ActionResult GerarFolha(int? id)
        {
            try
            {
                if (!id.HasValue || id == 0) throw new Exception("Informe o arquivo para gerar a folha de votação");
                var model = new FolhaVotacaoViewModel { ArquivoId = id.Value };
                //ViewBag.ArquivoId = id;
                //DataSet ds = arquivo.LerArquivo(arquivo.EnviarArquivo(fuArquivo, caminho), caminho);

                return View(model);
            }
            catch (Exception exception)
            {
                this.error(exception.Message);
                return View();
            }
        }


        [HttpPost]
        public FileResult Download(FolhaVotacaoViewModel model)
        {
            try
            {
                if (model.ArquivoId == 0)
                {
                    this.warning("Informe o arquivo para gerar a folha de votação");
                    return File(new byte[0], System.Net.Mime.MediaTypeNames.Application.Pdf, "invalido.pdf");
                }
                if (!ModelState.IsValid)
                {
                    this.warning("É necessário informar as informações corretamente!");
                    return File(new byte[0], System.Net.Mime.MediaTypeNames.Application.Pdf, "invalido.pdf");
                    //return RedirectToAction("Arquivos");
                }

                var arquivo = db.Arquivos.Find(model.ArquivoId);
                ViewBag.Arquivo = arquivo;
                var sheet = arquivo.LerArquivo(arquivo.Nome, arquivo.Caminho + "\\", out string city);
                model.Beneficiarios = arquivo.Buscar(sheet);
                switch (model.Unidade)
                {
                    case Unidade.BM:
                        model.Beneficiarios = model.Beneficiarios.Where(x => x.Re.StartsWith("2")).ToList();
                        break;
                    case Unidade.PM:
                        model.Beneficiarios = model.Beneficiarios.Where(x => x.Re.StartsWith("1")).ToList();
                        break;
                }

                if (model.Paginacao == Paginacao.Sim)
                {
                    var html = this.ToHtml("Imprimir", new ViewDataDictionary(model));

                    var pdf = Pdf
    .From(html)
    .OfSize(PaperSize.A4)
    .WithTitle(city)
    .WithoutOutline()
    .WithMargins(1.25.Centimeters())
    .Portrait()
    .Comressed()
    .Content();

                    return File(pdf, System.Net.Mime.MediaTypeNames.Application.Pdf, $"{city}.pdf");
                }
                //return new ViewAsPdf { Model = model, ViewName = "Imprimir" };
                if (model.Paginacao == Paginacao.Não)
                {

                    var html = this.ToHtml("ImprimirPage", new ViewDataDictionary(model));

                    var pdf = Pdf
    .From(html)
    .OfSize(PaperSize.A4)
    .WithTitle(city)
    .WithoutOutline()
    .WithMargins(1.25.Centimeters())
    .Portrait()
    .Comressed()
    .Content();
                    return File(pdf, System.Net.Mime.MediaTypeNames.Application.Pdf, $"{city}.pdf");
                }
                //return new ViewAsPdf { Model = model, ViewName = "ImprimirPage" };

                //return View("FolhaVotacao", model);
                return File(new byte[0], System.Net.Mime.MediaTypeNames.Application.Pdf, "invalido.pdf");
            }
            catch (Exception exception)
            {
                this.error(exception.Message);
                return File(new byte[0], System.Net.Mime.MediaTypeNames.Application.Pdf, "invalido.pdf");
            }
        }

        [HttpPost]
        public ActionResult GerarFolha(FolhaVotacaoViewModel model)
        {
            try
            {
                if (model.ArquivoId == 0)
                {
                    this.warning("Informe o arquivo para gerar a folha de votação");
                    return RedirectToAction("Arquivos");
                }
                if (!ModelState.IsValid)
                {
                    this.warning("É necessário informar as informações corretamente!");
                    return View(model);
                    //return RedirectToAction("Arquivos");
                }

                var arquivo = db.Arquivos.Find(model.ArquivoId);
                ViewBag.Arquivo = arquivo;
                var sheet = arquivo.LerArquivo(arquivo.Nome, arquivo.Caminho + "\\", out string city);
                model.Beneficiarios = arquivo.Buscar(sheet);
                switch (model.Unidade)
                {
                    case Unidade.BM:
                        model.Beneficiarios = model.Beneficiarios.Where(x => x.Re.StartsWith("2")).ToList();
                        break;
                    case Unidade.PM:
                        model.Beneficiarios = model.Beneficiarios.Where(x => x.Re.StartsWith("1")).ToList();
                        break;
                }

                if (model.Paginacao == Paginacao.Sim)
                {
                    return View("Imprimir", model);
                }
                //return new ViewAsPdf { Model = model, ViewName = "Imprimir" };
                if (model.Paginacao == Paginacao.Não)
                {
                    return View("ImprimirPage", model);
                }
                //return new ViewAsPdf { Model = model, ViewName = "ImprimirPage" };

                return View(model);
                //return View("FolhaVotacao", model);
            }
            catch (Exception exception)
            {
                this.error(exception.Message);
                return View();
            }
        }

        public ActionResult FolhaVotacao(FolhaVotacaoViewModel model)
        {
            try
            {
                return View(model);
            }
            catch (Exception exception)
            {
                this.error(exception.Message);
                return View();
            }
        }


        public ActionResult Imprimir(FolhaVotacaoViewModel model)
        {
            try
            {
                if (ModelState.IsValid) return View(model);
                this.warning("É necessário informar as informações corretamente!");
                return RedirectToAction("GerarFolha", model);
            }
            catch (Exception exception)
            {
                this.error(exception.Message);
                return View();
            }
        }


        /// <summary>
        /// Action responsável por realizar o UPLOAD dos arquivos informados.
        /// </summary>
        /// <returns>Retorna uma view com o total de arquivos enviados</returns>
        [HttpPost]
        public ActionResult FileUpload()
        {
            try
            {
                int arquivosSalvos = 0;
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase arquivo = Request.Files[i];

                    //Suas validações ......

                    //Salva o arquivo
                    if (arquivo.ContentLength > 0)
                    {
                        Arquivo arq = new Arquivo();
                        var uploadPath = Server.MapPath("~/Uploads");
                        string caminhoArquivo = Path.Combine(@uploadPath, Path.GetFileName(arquivo.FileName));
                        arq.Tipo = Path.GetExtension(caminhoArquivo);
                        arq.Caminho = uploadPath;
                        arq.Nome = arquivo.FileName;
                        arq.Tamanho = arquivo.ContentLength;
                        if (arq.Tipo.ToUpper() != ".XLS" && arq.Tipo.ToUpper() != ".XLSX") continue;
                        arquivo.SaveAs(caminhoArquivo);
                        db.Arquivos.Add(arq);
                        db.SaveChanges();
                        arquivosSalvos++;
                    }
                }

                ViewBag.Message = String.Format("{0} arquivo(s) salvo(s) com sucesso.", arquivosSalvos);
                return RedirectToAction("Arquivos");
            }
            catch (Exception exception)
            {
                this.error(exception.Message);
                return View();
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Sobre o sistema SIEL.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}